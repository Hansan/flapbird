CC = clang++
LIBS = -lSDL2
BINDIR = bin
BIN = flappy
SRC = src/core.cpp
CFLAGS = -std=c++17 -fno-exceptions -fno-rtti -Wall -Wextra -Wpedantic -Wshadow
DEFINES = -DUSESDL

ifeq ($(OS),Windows_NT)
	LIBS := -lmingw32 -lSDL2main $(LIBS) -lgdi32
	BIN := $(BIN).exe
endif

debug:
	${CC} ${CFLAGS} ${DEFINES} -g -o ${BINDIR}/${BIN} ${SRC} src/sdlmain.cpp ${LIBS}

windows:
	${CC} ${CFLAGS} -g -o ${BINDIR}/${BIN} ${SRC} src/winmain.cpp ${LIBS}

release:
	${CC} ${CFLAGS} ${DEFINES} -O2 -o ${BINDIR}/${BIN} ${SRC} src/sdlmain.cpp ${LIBS}

test: debug
	${BINDIR}/${BIN}

clean:
	rm ${BINDIR}/${BIN}

syntax:
	${CC} ${CFLAGS} -fsyntax-only ${SRC}
