#pragma once

#include "core.hpp"

#define swap_buffer sdl_swap_buffer
#define handle_input sdl_handle_input
#define get_back_buffer sdl_get_back_buffer
#define get_window_dimensions sdl_get_window_dimension

auto sdl_handle_input(Message* outMsg) -> bool;
auto sdl_get_back_buffer() -> BackBuffer;
auto sdl_swap_buffer() -> void;
auto sdl_get_window_dimension() -> V2;
