#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

#include <SDL2/SDL.h>

#include "jint.h"
#include "core.hpp"

struct Square {
    float x;
    float y;
    int w;
    int h;
};

const auto windoww = 800;
const auto windowh = 600;

SDL_Surface* gBBSurface;
SDL_Surface* gWinSurface;
SDL_Window* gWindow;

auto sdl_get_window_dimension() -> V2
{
    return { windoww, windowh };
}

auto sdl_swap_buffer() -> void
{
    SDL_BlitSurface(gBBSurface, NULL, gWinSurface, NULL);
    SDL_UpdateWindowSurface(gWindow);
}

auto sdl_get_back_buffer() -> BackBuffer
{
    auto bbuf = BackBuffer{};
    bbuf.memory = gBBSurface->pixels;
    bbuf.w = gBBSurface->w;
    bbuf.h = gBBSurface->h;
    bbuf.pitch = gBBSurface->pitch;
    bbuf.bpp = gBBSurface->format->BytesPerPixel;

    return bbuf;
}

auto sdl_handle_input(Message* outMsg) -> bool
{
    *outMsg = NONE;
    SDL_Event e;
    if (SDL_PollEvent(&e)) {
        if (e.type == SDL_QUIT) {
            *outMsg = QUIT;
        } else if (e.type == SDL_KEYDOWN) {
            switch (e.key.keysym.sym) {
            case SDLK_UP: {
                *outMsg = JUMP;
            } break;
            case SDLK_r: {
                *outMsg = RESET;
            } break;
            case SDLK_KP_PLUS: {
                *outMsg = INCREASE_FONT;
            } break;
            case SDLK_KP_MINUS: {
                *outMsg = DECREASE_FONT;
            } break;
            default: {
            } break;
            }
        }
    }

    return *outMsg ? true : false;
}

int main(int, char**)
{
    SDL_Init(SDL_INIT_EVERYTHING);

    gWindow = SDL_CreateWindow("The Tale of Flappy the Bird",
                               SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                               windoww, windowh, SDL_WINDOW_SHOWN);
    assert(gWindow);

    gWinSurface = SDL_GetWindowSurface(gWindow);
    assert(gWinSurface);

    gBBSurface = SDL_CreateRGBSurface(0, gWinSurface->w, gWinSurface->h,
                                      gWinSurface->format->BitsPerPixel,
                                      gWinSurface->format->Rmask,
                                      gWinSurface->format->Gmask,
                                      gWinSurface->format->Bmask,
                                      gWinSurface->format->Amask);
    assert(gBBSurface);

    run();

    return 0;
}
