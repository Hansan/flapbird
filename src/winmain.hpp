#pragma once

#include "core.hpp"

#define swap_buffer win32_swap_buffer
#define handle_input win32_handle_input
#define get_back_buffer win32_get_back_buffer
#define get_window_dimensions win32_get_global_window_dimension

auto win32_handle_input(Message* outMsg) -> bool;
auto win32_get_back_buffer() -> BackBuffer;
auto win32_swap_buffer() -> void;
auto win32_get_global_window_dimension() -> V2;
