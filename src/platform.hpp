#pragma once

#if defined(USESDL)
#include "sdlmain.hpp"
#elif defined(_WIN64) || defined(_WIN32)
#include "winmain.hpp"
#elif defined(__linux__)
#include "sdlmain.hpp" // Temporary
#else
#include "sdlmain.hpp" // Default: hope the system supports SDL2
#endif
