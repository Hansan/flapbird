#include <cassert>
#include <cstdint>
#include <cstdio>

#include <windows.h>

#include "jint.h"
#include "core.hpp"
#include "winmain.hpp"

struct win32_offscreen_buffer {
    BITMAPINFO info;
    void* memory;
    int w;
    int h;
    int pitch;
    int bpp;
};

// TODO: global for now
static win32_offscreen_buffer gBackBuf;
HWND gWindow;
const auto windoww = 800;
const auto windowh = 600;

extern bool gRunning;

auto win32_get_window_dimension(HWND window) -> V2
{
    // return the buffer dimensions for now since
    // the window doesn't match the buffer for some
    // reason
    auto result = V2{};

    /* auto clientRect = RECT {}; */
    /* GetClientRect(window, &clientRect); */
    /* result.w = clientRect.right - clientRect.left; */
    /* result.h = clientRect.bottom - clientRect.top; */

    result.w = gBackBuf.w;
    result.h = gBackBuf.h;

    return result;
}

auto win32_get_global_window_dimension() -> V2
{
    return win32_get_window_dimension(gWindow);
}

auto win32_resize_dib_section(win32_offscreen_buffer* buf, int w, int h)
{
    if (buf->memory) {
        VirtualFree(buf->memory, 0, MEM_RELEASE);
    }

    buf->w = w;
    buf->h = h;
    buf->bpp = 4;

    buf->info.bmiHeader.biSize = sizeof(buf->info.bmiHeader);
    buf->info.bmiHeader.biWidth = w;
    buf->info.bmiHeader.biHeight = -h;
    buf->info.bmiHeader.biPlanes = 1;
    buf->info.bmiHeader.biBitCount = 32;
    buf->info.bmiHeader.biCompression = BI_RGB;

    auto bitmapMemorySize = buf->bpp * w * h;
    buf->memory = VirtualAlloc(0, bitmapMemorySize, MEM_COMMIT, PAGE_READWRITE);

    buf->pitch = w * buf->bpp;
}

static void
win32_display_buffer_in_window(HDC deviceContext, int windowWidth, int windowHeight,
                               win32_offscreen_buffer buf, int x, int y, int w, int h)
{
    // TODO Aspect ratio correction
    StretchDIBits(deviceContext, 0, 0, buf.w, buf.h, 0, 0, buf.w, buf.h,
                  buf.memory, &buf.info, DIB_RGB_COLORS, SRCCOPY);
}

LRESULT CALLBACK
win32_main_window_callback(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
    auto result = LRESULT{};

    switch (message) {
    case WM_SIZE: {
    } break;
    case WM_CLOSE: {
        printf("CLOSE\n");
        gRunning = false;
    } break;
    case WM_ACTIVATEAPP: {
        fprintf(stderr, "WM_ACTIVATEAPP\n");
    } break;
    case WM_DESTROY: {
        printf("DESTROY\n");
        gRunning = false;
    } break;
    case WM_PAINT: {
        auto paint = PAINTSTRUCT{};
        auto deviceContext = BeginPaint(window, &paint);
        auto x = paint.rcPaint.left;
        auto y = paint.rcPaint.top;
        auto w = paint.rcPaint.right - paint.rcPaint.left;
        auto h = paint.rcPaint.bottom - paint.rcPaint.top;

        auto dimension = win32_get_window_dimension(window);

        win32_display_buffer_in_window(deviceContext, dimension.w, dimension.h,
                                       gBackBuf, x, y, w, h);
        EndPaint(window, &paint);
    } break;
    default: {
        // fprintf(stderr, "default\n");
        result = DefWindowProc(window, message, wParam, lParam);
    } break;
    }

    return result;
}

auto win32_handle_input(Message* outMsg) -> bool
{
    *outMsg = NONE;
    auto winMsg = MSG{};
    if (PeekMessage(&winMsg, 0, 0, 0, PM_REMOVE)) {
        if (winMsg.message == WM_QUIT) {
            *outMsg = QUIT;
        } else if (winMsg.message == WM_KEYDOWN) {
            switch (winMsg.wParam) {
            case VK_UP: {
                *outMsg = JUMP;
            } break;
            case 0x52: { // R key
                *outMsg = RESET;
            } break;
            default: {
            } break;
            }
        }
        TranslateMessage(&winMsg);
        DispatchMessage(&winMsg);
    }

    return *outMsg ? true : false;
}

auto win32_get_back_buffer() -> BackBuffer
{
    auto bbuf = BackBuffer{};
    bbuf.memory = gBackBuf.memory;
    bbuf.w = gBackBuf.w;
    bbuf.h = gBackBuf.h;
    bbuf.pitch = gBackBuf.pitch;
    bbuf.bpp = gBackBuf.bpp;

    return bbuf;
}

auto win32_swap_buffer() -> void
{
    auto deviceContext = GetDC(gWindow);

    auto dimension = win32_get_window_dimension(gWindow);
    win32_display_buffer_in_window(deviceContext, dimension.w, dimension.h,
                                   gBackBuf, 0, 0, dimension.w, dimension.h);
    ReleaseDC(gWindow, deviceContext);
}

int CALLBACK
WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR commandLine, int showCode)
{
    auto windowClass = WNDCLASS{};

    win32_resize_dib_section(&gBackBuf, windoww, windowh);

    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = win32_main_window_callback;
    windowClass.hInstance = instance;
    //windowClass.hIcon;
    windowClass.lpszClassName = "HandmadeHeroWindowClass";

    assert(RegisterClass(&windowClass));

    gWindow = CreateWindowEx(0, windowClass.lpszClassName, "Handmade Hero",
                             WS_OVERLAPPEDWINDOW | WS_VISIBLE, CW_USEDEFAULT,
                             CW_USEDEFAULT, windoww, windowh, 0, 0, instance, 0);
    assert(gWindow);

    run();

    return 0;
}
