#include <array>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>

#include "jint.h"
#include "core.hpp"
#include "platform.hpp"

#define STB_TRUETYPE_IMPLEMENTATION
#define STB_IMAGE_IMPLEMENTATION
#include "stb_truetype.h"
#include "stb_image.h"

struct Square {
    float x;
    float y;
    int w;
    int h;
};

struct Point {
    float x;
    float y;
};

auto gRunning = false;

auto draw_solid_square(BackBuffer* buf, Square sqr, uint r, uint g, uint b)
{
    for (auto y = 0; y < sqr.h; ++y) {
        auto pixely = (int)sqr.y + y;
        if (pixely < 0 || pixely >= buf->h) {
            continue;
        }
        for (auto x = 0; x < sqr.w; ++x) {
            auto pixelx = (int)sqr.x + x;
            if (pixelx < 0 || pixelx >= buf->w) {
                continue;
            }

            auto currbyteindex = pixely * buf->w + pixelx;
            auto currbyte = ((u8*)buf->memory + currbyteindex * buf->bpp);

            *currbyte++ = b;
            *currbyte++ = g;
            *currbyte++ = r;
        }
    }
}

auto draw_image(BackBuffer* backBuf, Point dest, BackBuffer* img)
{
    for (auto y = 0; y < img->h; ++y) {
        auto pixely = (int)dest.y + y;
        if (pixely < 0 || pixely >= backBuf->h) {
            continue;
        }
        for (auto x = 0; x < img->w; ++x) {
            auto pixelx = (int)dest.x + x;
            if (pixelx < 0 || pixelx >= backBuf->w) {
                continue;
            }

            auto currBBbyteindex = pixely * backBuf->w + pixelx;
            auto currBBbyte = ((u8*)backBuf->memory + currBBbyteindex * backBuf->bpp);
            auto currimgbyteindex = y * img->w + x;
            auto currimgbyte = ((u8*)img->memory + currimgbyteindex * img->bpp);

            auto r = *currimgbyte++;
            auto g = *currimgbyte++;
            auto b = *currimgbyte++;
            auto a = *currimgbyte++;

            // FIXME: hack
            if (!a) {
                continue;
            }

            *currBBbyte++ = b;
            *currBBbyte++ = g;
            *currBBbyte++ = r;
        }
    }
}

auto draw_text(BackBuffer& backBuf, stbtt_fontinfo& font, int fontHeight, const char* text, int textX, int textY)
{
    float currentChX = textX;

    auto scale = stbtt_ScaleForPixelHeight(&font, fontHeight);
    int ascent = 0;
    stbtt_GetFontVMetrics(&font, &ascent, 0, 0);

    for (size_t chindex = 0; chindex < strlen(text); ++chindex) {
        auto letter = text[chindex];
        auto nextLetter = text[chindex + 1];
        int advance;
        int lsb;
        stbtt_GetCodepointHMetrics(&font, letter, &advance, &lsb);
        int w, h, xoff, yoff;
        auto bitmap = stbtt_GetCodepointBitmap(&font, 0, scale, letter, &w, &h, &xoff, &yoff);

        auto sqr = Square{
            currentChX + xoff, textY + (float)(fontHeight + yoff), w, h
        };

        currentChX += (advance * scale);
        if (nextLetter) {
            currentChX += scale * stbtt_GetCodepointKernAdvance(&font, letter, nextLetter);
        }

        for (auto y = 0; y < sqr.h; ++y) {
            for (auto x = 0; x < sqr.w; ++x) {

                auto pixely = (int)sqr.y + y;
                auto pixelx = (int)sqr.x + x;
                if (pixely < 0 || pixely >= backBuf.h || pixelx < 0 || pixelx >= backBuf.w) {
                    continue;
                }

                auto fontbitmapindex = y * sqr.w + x;

                auto bbindex = pixely * backBuf.w + pixelx;
                auto currbyte = ((u8*)backBuf.memory + bbindex * backBuf.bpp);

                // whoo custom alpha blending
                auto alphaLevel = bitmap[fontbitmapindex];

                auto b = *(currbyte + 0);
                auto g = *(currbyte + 1);
                auto r = *(currbyte + 2);

                auto alphadb = b - (alphaLevel / 255.0 * b);
                auto alphadg = g - (alphaLevel / 255.0 * g);
                auto alphadr = r - (alphaLevel / 255.0 * r);

                *currbyte++ = alphadb;
                *currbyte++ = alphadg;
                *currbyte++ = alphadr;
            }
        }

        stbtt_FreeBitmap(bitmap, nullptr);
    }
}

auto openings = std::array<Square, 4>{};
auto guy = Square{};
auto jumpspeed = 0;
auto velocity = 0.0;
auto gravity = 0;

auto scroll = 0.0;
auto scrollspeed = 0;

auto score = 0.0;
auto lost = false;

auto currentclock = decltype(clock()){ 0 };
auto delta = 0.0;

stbtt_fontinfo font;
auto fontHeight = 20;
auto fontRatio = 0.f;

auto highScore = 0;

auto init()
{
    auto winDimensions = get_window_dimensions();
    guy.w = 10;
    guy.h = 10;
    guy.x = winDimensions.w / 3;
    guy.y = winDimensions.h / 3;

    jumpspeed = 600;
    velocity = 0.0;
    gravity = 1500;

    scroll = 0.0;
    scrollspeed = 200;

    score = 0.0;
    lost = false;

    delta = 0.0;
    currentclock = clock();

    srand(time(NULL));

    auto i = 0;
    for (auto& it : openings) {
        it.w = 20;
        it.h = 150;
        it.x = winDimensions.w + i++ * (winDimensions.w / 4);
        it.y = (float)(rand() % (winDimensions.h - it.h));
    }

    if (static auto fontinited = false; !fontinited) {
        static uchar ttf_buffer[1 << 25];

        fread(ttf_buffer, 1, 1 << 25, fopen("fonts/DejaVuSansMono.ttf", "rb"));

        stbtt_InitFont(&font, ttf_buffer, stbtt_GetFontOffsetForIndex(ttf_buffer, 0));

        auto fontScale = stbtt_ScaleForPixelHeight(&font, fontHeight);
        int advance;
        stbtt_GetCodepointHMetrics(&font, 'a', &advance, nullptr);
        auto fontWidth = advance * fontScale;
        fontRatio = fontWidth / fontHeight;

        fontinited = true;
    }
}

auto run() -> void
{
    auto bb = get_back_buffer();
    gRunning = true;

    auto winDimensions = get_window_dimensions();
    auto windoww = winDimensions.w;
    auto windowh = winDimensions.h;

    init();

    while (gRunning) {
        auto newclock = clock();
        auto frameclocktime = newclock - currentclock;
        currentclock = newclock;

        delta = (double)frameclocktime / CLOCKS_PER_SEC;
        auto framemstime = 1000.0 * delta;

        static int c = 0;

        // TODO: sleep so cpu doesn't melt

        // input
        auto message = Message{};
        while (handle_input(&message)) {
            if (message == QUIT) {
                gRunning = false;
            } else if (message == JUMP) {
                velocity = jumpspeed;
            } else if (message == RESET) {
                init();
            } else if (message == INCREASE_FONT) {
                fontHeight += 1;
            } else if (message == DECREASE_FONT) {
                fontHeight -= 1;
            }
        }

        // simulation

        velocity -= gravity * delta;
        guy.y -= velocity * delta;
        if (guy.y < 0.0) {
            guy.y = 0.0;
            velocity = 0.0;
        } else if (guy.y + guy.h > bb.h) {
            lost = true;
            std::cerr << "You lose";
            guy.y = bb.h - guy.h;
        }

        auto scrolldelta = scrollspeed * delta;
        scroll += scrolldelta;
        // TODO: reset scroll so it doesn't get big and lose precision

        auto guycolor = 0xff;
        for (auto& it : openings) {
            // update position
            it.x -= scrolldelta;
            if (it.x + it.w < 0) {
                it.x = windoww;
                it.y = (float)(rand() % (windowh - it.h));
            }

            // check for collision
            if (guy.x < it.x + it.w && guy.x + guy.w > it.x && (guy.y < it.y || guy.y + guy.h > it.y + it.h)) {
                lost = true;
            }
        }

        if (!lost) {
            score += scrolldelta;
            if (score > highScore) highScore = score;
        } else {
            gravity = 0;
            velocity = 0;
            jumpspeed = 0;
            scrollspeed = 0;
            guycolor = 0;
        }

        // draw background
        for (auto y = 0; y < bb.h; ++y) {
            for (auto x = 0; x < bb.w; ++x) {
                auto currbyteindex = y * bb.w + x;
                auto currbyte = ((u8*)bb.memory + currbyteindex * bb.bpp);
                // b
                *currbyte++ = y;
                // g
                *currbyte++ = (int)(x + scroll * 0.7);
                // r
                *currbyte++ = 0;
            }
        }

        for (auto& it : openings) {
            draw_solid_square(&bb, it, 0xff, 0, 0);
            auto top = it;
            top.y = 0;
            top.h = it.y;
            draw_solid_square(&bb, top, 0, 0xff, 0xff);

            auto bot = it;
            bot.y += bot.h;
            bot.h = bb.h - bot.y;
            draw_solid_square(&bb, bot, 0, 0xff, 0xff);
        }

        // test draw font
        {
            char strbuf[128] = {};
            snprintf(strbuf, 128, "Score: %05llu", (ullong)score);
            draw_text(bb, font, fontHeight, strbuf, 20, 10);

            snprintf(strbuf, 128, "Hiscore: %05llu", (ullong)highScore);

            // the font is monospaced, so the width of the entire string can be found
            draw_text(bb, font, fontHeight, strbuf, bb.w - strlen(strbuf) * (fontHeight * fontRatio) - 20, 10);

            if (lost) {
                auto newFontHeight = fontHeight * 2;
                auto fontWidth = newFontHeight * fontRatio;
                snprintf(strbuf, 128, "You lost. press 'r' to continue!", (ullong)score, (ullong)highScore);
                draw_text(bb, font, newFontHeight, strbuf,
                          bb.w / 2. - (strlen(strbuf) * fontWidth) / 2.,
                          bb.h / 2. - fontHeight / 2.);
            }
        }

        BackBuffer guyTexture = {};
        guyTexture.bpp = 4;
        guyTexture.memory = stbi_load("res/guy.bmp", &guyTexture.w, &guyTexture.h, nullptr, guyTexture.bpp);
        if (!guyTexture.memory) {
            fprintf(stderr, "couldn't find texture 'res/guy.bmp'");
            assert(0);
        }
        draw_image(&bb, {guy.x, guy.y}, &guyTexture);
        /* draw_solid_square(&bb, guy, guycolor, 0, 0xff); */
        swap_buffer();
        free(guyTexture.memory);
    }
}
