#pragma once

struct BackBuffer {
    void* memory;
    int w;
    int h;
    int pitch;
    int bpp;
};

struct V2 {
    int w;
    int h;
};

enum Message {
    NONE,
    QUIT,
    JUMP,
    RESET,
    INCREASE_FONT,
    DECREASE_FONT,
};

auto run() -> void;
