This is a flappy bird clone.

Controls
--------

* UP: Jump
* R: Reset
* Keypad Plus: Increase font size
* Keypad Minus: Decrease font size

Dependencies
------------

* SDL2 (Optional on Windows)
